import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		Snake[] pit = new Snake[1];
		
		for(int i = 0; i<pit.length;i++){
			
			System.out.println("What type of snake is it?");
			String inputType = reader.nextLine();
			System.out.println("How long is it?");
			int inputLength = Integer.parseInt(reader.nextLine());
			System.out.println("What is it's colour?");
			String inputColour = reader.nextLine();
			
		 pit[i] = new Snake(inputLength,inputType,inputColour);
			
		}
		for(int i = 0; i<pit.length;i++){
			
			System.out.println(pit[i].getLength());
			System.out.println(pit[i].getType());
			System.out.println(pit[i].getColour());
		}
		
		System.out.println("Update the length of the last snake.");
		pit[pit.length-1].setLength(Integer.parseInt(reader.nextLine()));
		
		System.out.println(pit[pit.length-1].getType());
		System.out.println(pit[pit.length-1].getColour());
		System.out.println(pit[pit.length-1].getLength());
		
	}
}