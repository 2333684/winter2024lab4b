import java.util.Random;
public class Snake{
	private int length;
	private String type;
	private String colour;
	
	public void setLength(int length) {
		this.length = length;
	}

	
	public int getLength() {
		return this.length;
	}
	public String getType() {
		return this.type;
	}
	public String getColour() {
		return this.colour;
	}
	
	public Snake(int length, String type, String colour){
		this.length = length;
		this.type = type;
		this.colour = colour;
	}
	
	Random rand = new Random();
	
	public void eat(){
		int num = rand.nextInt(3);
		if(sleep){
			System.out.println(this.type + " is asleep, they cannot go eat");
		}else if(num == 0){
			System.out.println(this.type + " ate a mouse");
		}else if(num == 1){
			System.out.println(this.type + " ate a bunny");
		}else{
			System.out.println(this.type + " failed to catch it's prey");
		}
	}
	
	boolean sleep = false;
	
	public void sleep(){
		sleep = true;
		if(sleep){
			System.out.println(this.type + " is now asleep");
		}else{
			System.out.println(this.type + " hath awoken!");
		}
	}
}